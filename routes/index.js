var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  let students = ['Eden', "Refael", 'Yoni', 'Nitzan', 'Hadas'];
  let attendees = [ ['Eden', 'Refael', 'Yoni', 'Nitzan', 'Hadas', 'Ortal'],
                    ['Berry', 'Nitzan', 'Yoni', 'Eden', 'Hadas', 'Ortal'],
                    ['Maxim', 'Ortal', 'Yoni', 'Refael', 'Nitzan', 'Alex'],
                    ['Eden', 'Andrew', 'Yoni', 'Nitzan', 'Ortal','Nitzan', 'Andrew', 'Andrew', 'Andrew', 'Andrew', 'Andrew']];
  let N = 3

  let topNStudents = topNStudentsAttendees(students, attendees, N)

  res.render('index', { title: 'Express' , answer:topNStudents});
});

module.exports = router;

const topNStudentsAttendees = (students, attendees, N) => {
  if (!Array.isArray(students)) {return "bad students param";}
  if (!Array.isArray(attendees)) {return "bad attendees param";}
  if (!Number.isInteger(N)) {return "bad N param";}
  const studentsAttendance = new Map();

  // init the map of student attendance in O(S)
  students.forEach(function initEmptyStudentsMap(student) {
    studentsAttendance.set(student, new Set());
  });

  // O(M)
  for (let lectureNumber = 0; lectureNumber < attendees.length; lectureNumber++) {
  // O(S)
    attendees[lectureNumber].forEach(function addLectureNumberToAllStudents(studentName) {
      if (studentsAttendance.has(studentName)) {
        // get is O(1) in map
        studentsAttendance.get(studentName).add(lectureNumber);
      }
    });
  }

  // sort operation O(SLogS)
  const sortStringValues = (a, b) => a[1].size > b[1].size;
  let sortedStudentsAttendance = [...studentsAttendance].sort((a, b) => b[1].size - a[1].size);

  // output values as requested in the Exam
  let mapOfRequestedNumberOfTopParticipants = new Map(sortedStudentsAttendance.slice(0, N));
  return Array.from(mapOfRequestedNumberOfTopParticipants.keys());
}
